package oma.intern.ussd.screen;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class USSDResponse {

    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("end")
    @Expose
    private Boolean end;
    @SerializedName("libNext")
    @Expose
    private String libNext;
    @SerializedName("libReturn")
    @Expose
    private String libReturn;
    @SerializedName("parameter")
    @Expose
    private final Map<String, Object> parameter = new HashMap<>();
    @SerializedName("libMenu")
    @Expose
    private String libMenu;
    @SerializedName("nextMenu")
    @Expose
    private final Map<String, Object> nextMenu = new HashMap<>();
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("formAction")
    @Expose
    private String formAction;
    @SerializedName("returnMenu")
    @Expose
    private String returnMenu;

    public USSDResponse() {

    }

    public USSDResponse(Map<String, Object> parameters) {
        parameters.forEach(
            (key, value) -> {
                this.parameter.put(key, value);
                if("libReturn".equals(key))
                    this.libReturn = value.toString();
                if("libNext".equals(key))
                    this.libNext = value.toString();
                if("libMenu".equals(key))
                    this.libMenu = value.toString();
            }
        );
    }

    public USSDResponse(String libNext, String libReturn, String libMenu) {
        this.libNext = libNext;
        this.libReturn = libReturn;
        this.libMenu = libMenu;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean getEnd() {
        return end;
    }

    public void setEnd(Boolean end) {
        this.end = end;
    }

    public String getLibNext() {
        return libNext;
    }

    public void setLibNext(String libNext) {
        this.libNext = libNext;
    }

    public String getLibReturn() {
        return libReturn;
    }

    public void setLibReturn(String libReturn) {
        this.libReturn = libReturn;
    }

    public Map<String, Object> getParameter() {
        return parameter;
    }

    public String getLibMenu() {
        return libMenu;
    }

    public void setLibMenu(String libMenu) {
        this.libMenu = libMenu;
    }

    public Map<String, Object> getNextMenu() {
        return nextMenu;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFormAction() {
        return formAction;
    }

    public void setFormAction(String formAction) {
        this.formAction = formAction;
    }

    public String getReturnMenu() {
        return returnMenu;
    }

    public void setReturnMenu(String returnMenu) {
        this.returnMenu = returnMenu;
    }
}