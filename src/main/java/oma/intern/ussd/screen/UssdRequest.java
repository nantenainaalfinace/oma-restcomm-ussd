package oma.intern.ussd.screen;

import java.util.Map;

public class UssdRequest {
    private String msisdn;
    private String session;
    private String replay;
    private String racineCode;
    private Map<String,Object> parameter;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getReplay() {
        return replay;
    }

    public void setReplay(String replay) {
        this.replay = replay;
    }

    public String getRacineCode() {
        return racineCode;
    }

    public void setRacineCode(String racineCode) {
        this.racineCode = racineCode;
    }

    public Map<String, Object> getParameter() {
        return parameter;
    }

    public void setParameter(Map<String, Object> parameter) {
        this.parameter = parameter;
    }
}